export default interface Mquery {
    filter: Object;
    paginate: {
        limit: number;
        skip: number;
        page: number;
    },
    populate: {
        path: string;
        select: Object
    }[];
    select: {
        number: number;
        amount: number
    };
    sort: any
}
