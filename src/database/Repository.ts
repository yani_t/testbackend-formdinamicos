import mongoose from "mongoose";
import Persistent from "../entities/Persistent";
import Mquery from "../types/Mquery";

class Repository<T extends Persistent> {
  model!: mongoose.Model<any>;

  constructor(model: mongoose.Model<any>) {
    this.model = model;
  }

  population(
    query: mongoose.Query<
      (mongoose.Document<any, any, any> & {
        _id: any;
      })[],
      mongoose.Document<any, any, any> & {
        _id: any;
      },
      {},
      mongoose.Document<any, any, any>
    >
  ) {
    return query;
  }

  async create(item: T): Promise<mongoose.Document<any, any, any>> {
    return this.model.create(item.getProps());
  }

  async insertMany(
    items: T[]
  ): Promise<(mongoose.Document<any, any, any> & { _id: any })[]> {
    return this.model.insertMany(items.map((item) => item.getProps()));
  }

  async findAll(mquery?: Partial<Mquery>) {
    const { limit, page, skip } = mquery?.paginate || {
      limit: 100,
      page: 1,
      skip: 0,
    };

    if (limit === 0) {
      return [];
    }

    return this.population(this.model.find({}).limit(limit).skip(skip));
  }

  async update(id: string, item: T) {
    return this.model.updateOne({ _id: id }, item.getProps());
  }

  async findById(id: string) {
    return this.population(this.model.find({ _id: id }));
  }

  async find(
    filter: mongoose.FilterQuery<mongoose.Document>,
    mquery?: Partial<Mquery>
  ) {
    const { limit, page, skip } = mquery?.paginate || {
      limit: 100,
      page: 1,
      skip: 0,
    };

    if (limit === 0) {
      return [];
    }

    const sort = mquery?.sort || { _id: 1 };
    return this.population(this.model.find(filter).sort(sort).limit(limit).skip(skip));
  }

  async findMquery(mquery: mongoose.FilterQuery<mongoose.Document>) {
    return this.population(this.model.find(mquery.filter))
      .limit(mquery.limit)
      .skip(mquery.skip);
  }

  async mquery(mquery: mongoose.FilterQuery<mongoose.Document>) {
    let find = this.model
      .find(mquery.filter)
      .limit(mquery.limit)
      .skip(mquery.skip);
    if (!mquery.populate) {
      find = this.population(find);
    }

    if (mquery.populate) {
      find.populate(mquery.populate);
    }

    if (mquery.select) {
      find.select(mquery.select);
    }

    if (mquery.sort) {
      find.sort(mquery.sort);
    }
    return find;
  }

  delete(id: string) {
    return this.model.findByIdAndDelete(id);
  }

  deleteMany(filter: mongoose.FilterQuery<mongoose.Document>) {
    return this.model.deleteMany(filter);
  }

  countDocuments(filter: mongoose.FilterQuery<mongoose.Document>) {
    return this.model.countDocuments(filter);
  }
}

export default Repository;
