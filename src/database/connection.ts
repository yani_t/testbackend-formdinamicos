import { MongoClient, MongoClientOptions } from 'mongodb';
import dotenv from 'dotenv';
import mongoose from 'mongoose';

dotenv.config();

const url = process.env.DB_URL || '';

export async function connect() {
  try {
    const client  = await mongoose.connect(url, {
      autoIndex: true,
    });
    console.log('Conexión a la base de datos exitosa');
    return client;
  } catch (error) {
    console.error('Error al conectar a la base de datos:', error);
    throw error;
  }
}
