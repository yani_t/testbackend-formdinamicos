import {  } from 'mongoose';
import Persistent from '../../../entities/Persistent';

export type UserDocument = User & Document;

export class User extends Persistent {
  name!: string;

  email!: string;

  password!: string;
  
  esAdmin?: boolean;

  constructor(data: Partial<User>) {
    super();
    Object.assign(this, data);
  }

  getProps(): object {
    return {
      name: this.name,
      email: this.email,
      password: this.password,
      esAdmin: this.esAdmin
    }
  }
}