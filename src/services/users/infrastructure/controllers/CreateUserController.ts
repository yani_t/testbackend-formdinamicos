import { NextFunction, Response } from "express";
import UserRepository from "../../../users/infrastructure/repository/UserRepository";
import { User } from "../../domain/User";

const createUser = async (req: any, resp: Response, next: NextFunction) => {
  try {
    const user = new User(req.body);
    await UserRepository.create(user);
    return resp.status(201).json({ result: { user }})
  }
  catch (error) {
    next(error);
  };
};

export default createUser;
