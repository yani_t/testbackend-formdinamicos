import express, { NextFunction, Request, Response } from "express";
import createUser from "../controllers/CreateUserController";
const routerPeticion = express.Router();

routerPeticion.post(
  "/users",
  (request: any, response: Response, next: NextFunction) => {
    createUser(request, response, next);
  }
);

export default routerPeticion;