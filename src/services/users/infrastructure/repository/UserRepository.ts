import Repository from "../../../../database/Repository";
import { User } from "../../../users/domain/User";

import UserModel from "../../domain/UserModel";

class UserRepository extends Repository<User> {
  constructor() {
    super(UserModel);
  }
}

export default new UserRepository();
