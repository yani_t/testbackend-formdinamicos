import express from 'express';
import { connect } from './database/connection';
import routerPeticion from './services/users/infrastructure/routes';

const app = express();
const port = 3000;

app.use(express.json({ limit: "50mb" }));

app.use(express.urlencoded({ extended: true, limit: "50mb" }));

// app.use(mquery({ limit: 100, maxLimit: 1000 }));

//Rutas y middlewares
app.get('/', (req, res) => {
  res.send('¡Hola, mundo!');
});

const server = app.use("/", routerPeticion);

app.listen(port, async() => {
  console.log(`Servidor escuchando en el puerto ${port}`);
  await connect();
});


export default server;