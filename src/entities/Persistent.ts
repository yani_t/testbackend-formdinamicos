abstract class Persistent {
    id!: string;
    getProps(): object {
      return {};
    }
  
    getId(): string {
      return this.id;
    }
  
    setId(id: string) {
      this.id = id;
    }
  }
  
  export default Persistent;